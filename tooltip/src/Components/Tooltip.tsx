import { useState } from 'react';
import styles from './Tooltip.module.css';

interface ToolTipProps {
  position: 'top' | 'right' | 'bottom' | 'left';
  content: string;
}

const Tooltip: React.FC<ToolTipProps> = (props: any) => {
  const [display, setDisplay] = useState(false);
  let positionStyle;

  if (props.position === 'top') positionStyle = styles.top;
  if (props.position === 'right') positionStyle = styles.right;
  if (props.position === 'bottom') positionStyle = styles.bottom;
  if (props.position === 'left') positionStyle = styles.left;

  return (
    <div
      className={`${styles.tooltipContainer}`}
      onMouseEnter={() => setDisplay(true)}
      onMouseLeave={() => setDisplay(false)}
    >
      {props.children}
      {display && (
        <div className={`${styles.tooltipContent} ${positionStyle}`}>
          {props.content}
        </div>
      )}
    </div>
  );
};

export default Tooltip;
