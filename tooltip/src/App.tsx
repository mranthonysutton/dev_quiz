import './App.css';
import Tooltip from './Components/Tooltip';

function App() {
  return (
    <div style={{ width: '300px', margin: '300px auto' }}>
      <Tooltip position="left" content="What text the tooltip should display!">
        Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro.
        De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo
        vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris.
        Hi mindless mortuis soulless creaturas, imo evil stalking monstra
        adventus resi dentevil vultus comedat cerebella viventium.
      </Tooltip>
      <div style={{ paddingTop: '2rem' }}>
        <Tooltip position="bottom" content="Another smaller tooltip">
          in hac habitasse platea dictumst vestibulum rhoncus est pellentesque
          elit ullamcorper dignissim
        </Tooltip>
      </div>
    </div>
  );
}

export default App;
