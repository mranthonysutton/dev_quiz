# Developer Quiz

## Remove Quotes

I wrote the small function with TypeScript, though it was a very simple project, I wanted to ensure that only a string would be added as an argument for the function. At first, I implemented a solution that would loop through an array of invalid characters that would want to be removed from the phrase. I used the `replaceAll` function within JavaScript and quickly realized that depending on what Node version is being used, the solution may not work.

Afterward, I figured using regex would probably be a much cleaner solution to replace any invalid characters.

## Tooltip Component

Used create react app and added TypeScript. I used TypeScript with react so that I could create an interface of props that would be required to make the tooltip work. The idea would be to wrap the Tooltip component around a given element. Depending on where you would want to add the tooltip, the props accept a `position` to either display to the left, right, top, or bottom of the element. Another prop that was added is the `content` that you could add a string that would be the tooltip text.

This tooltip could easily be further implemented to handle more "customization". For example, maybe I would want a larger tooltip, or set the tooltip to be displayed by default and then change the behavior of how the tooltip is closed besides mouse enter and mouse leave.

## Node Server

The node server I created is extremely simple. I mostly just wanted to showcase how middleware can be implemented to change some of the basic functionality of a server. I implemented the "whitelist" idea. Only valid addresses can access some of the routes. One of the routes implemented is open to the public. We may not want to always have protected routes so having the ability to determine which routes require specific access is what makes middleware such a powerful tool. Also, I wrapped the entire server in a logging middleware that will generate error logs, and combined logs, and record some of the information from the request headers.

Though I just sent everything back to the client via JSON, if needed we can also use Node to display static webpages and resources.

## Future of Docker Containers

I believe that services such as Kubernetes and Docker Swarm will continue to exist for quite some time. Before docker container services, multiple services would need to be implemented to run multiple services. Though these container services can be tricky to set up and implement if you are just starting, they are much easier for continuous integration and management over an enterprise.

I think we are going to continue to see more and more cloud services like AWS or DigitalOcean with docker services compared to VPS due to how they can scale. Containers also make having multiple environments such as staging, QA, production, etc. easier to maintain.
