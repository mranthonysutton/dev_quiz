const removeQuotes = (phrase: any) => {
  if (phrase.length === 0) throw new Error('Phrase can not be left empty.');
  if (typeof phrase !== 'string') throw new Error('Phrase must be a string');

  phrase = phrase.replace(/["']/g, '');

  return phrase;
};

console.log(removeQuotes('\'tes"ter"'));
console.log(removeQuotes('"thi\'s "should work""!\''));
