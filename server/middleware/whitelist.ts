import { Request, Response, NextFunction } from 'express';
import logger from './logger';

const validAddresses = [`localhost:${process.env.PORT}`];

const whitelist = (req: Request, _: Response, next: NextFunction) => {
  const origin = req.get('host');
  if (validAddresses.indexOf(origin) !== -1) {
    next();
  } else {
    logger.error({
      message: 'Whitelisted Route',
      headers: { ...req.headers }
    });
    throw new Error('Unable to connect to the requested resource');
  }
};

export default whitelist;
