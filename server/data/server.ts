import morgan from 'morgan';
import express, { Request, Response } from 'express';
import expressWinston from 'express-winston';
import logger from '../middleware/logger';
import whitelist from '../middleware/whitelist';

const server = express();
server.use(expressWinston.logger({ winstonInstance: logger }));
server.use(morgan('dev'));

server.use('/api/whitelist', whitelist, (_: Request, res: Response) => {
  res.json({ message: 'Congratulations, you have access.' });
});

server.use('/', (_: Request, res: Response) => {
  res.json({ message: 'API up and running...' });
});

export default server;
